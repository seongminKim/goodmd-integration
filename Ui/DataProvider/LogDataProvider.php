<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-06-13
 * Time: 오전 9:17
 */
namespace Eguana\GoodMD\Ui\DataProvider;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\App\RequestInterface;
use Eguana\GoodMD\Model\ResourceModel\Log\Grid\CollectionFactory;

/**
 * Class BannerDataProvider
 * @package Eguana\Slider\Model\Banner
 */
class LogDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    private $_coreRegistry;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $request
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        \Magento\Framework\Registry $coreRegistry,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->request = $request;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = [];
        $goodMDLogModel = $this->_coreRegistry->registry('eguana_goodmd_log');

        if (!empty($goodMDLogModel)) {
            $data[$goodMDLogModel->getId()] = $goodMDLogModel->getData();
        } else {
            $id = $this->request->getParam($this->getRequestFieldName());
            foreach ($this->getCollection()->getItems() as $log) {
                if ($id == $log->getId()) {
                    $data[$id] = $log->getData();
                }
            }
        }
        return $data;
    }
}
