<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-08-06
 * Time: 오전 10:18
 */

namespace Eguana\GoodMD\Block\Adminhtml\Items\Column;

/**
 * class DefaultColumn
 */
class DefaultColumn extends \Magento\Sales\Block\Adminhtml\Items\Column\DefaultColumn
{
    /**
     * Return I/F message to sales order view
     * @param $value
     * @return string
     */
    public function getLabelByValue($value)
    {
        $result ='Pending';

        if ($value == '02') {
            $result = 'Success';
        } elseif ($value == '03') {
            $result = 'Fail';
        } elseif ($value == '01') {
            $result ='Pending';
        }
        return $result;
    }
}