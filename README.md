Eguana_GooDMD v0.1.2

`Website` : CosRx

`Author` : Glenn

`Table` : eguana_goodmd_log

***Short Description***

Interface Module between Eguana and Shinsegae GoodMD
Order I/F, Product Master I/F, Stock I/F, Shipment I/F

***Explanation*** 

>1. Get Admin Token from Magento Api for authentication.
>
>2. Call Custom Api with json format data.
>
>3. Admin token should be contain in HEADER.
   ex)
   authorization Bearer adminToken
>4. Response will be json format as well.   
  
***History***

Upgrade v0.1.1
- add eguana_goodmd_log table
- add interface_status column to sales_order_item table

Upgrade v0.1.2
- add message column to eguana_goodmd_log table

