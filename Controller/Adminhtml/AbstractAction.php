<?php
/**
 * Created by PhpStorm.
 * User: glenn
 * Date: 2019-06-13
 * Time: 오후 5:08
 */

namespace Eguana\GoodMD\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;

abstract class AbstractAction extends Action
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Eguana_GoodMD::log';

    /**
     * Init page
     *
     * @param Page $resultPage
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Eguana_GoodMD::log')
            ->addBreadcrumb(__('GoodMD'), __('GoodMD'))
            ->addBreadcrumb(__('Log'), __('Log'));
        return $resultPage;
    }
}
