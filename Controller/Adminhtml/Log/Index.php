<?php
/**
 * Created by PhpStorm.
 * User: glenn
 * Date: 2019-06-13
 * Time: 오후 5:08
 */

namespace Eguana\GoodMD\Controller\Adminhtml\Log;

use Eguana\GoodMD\Controller\Adminhtml\AbstractAction;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;

/**
 * class Index
 *
 * GoodMD Admin menu controller for log
 */
class Index extends AbstractAction
{
    /**
     * class execute
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage
            ->setActiveMenu('Eguana_GoodMD::log')
            ->getConfig()->getTitle()->prepend(__('Log'));

        return $resultPage;
    }
}
