<?php
/**
 * Created by PhpStorm.
 * User: glenn
 * Date: 2019-06-13
 * Time: 오후 5:08
 */
namespace Eguana\GoodMD\Controller\Adminhtml\Log;

use Eguana\GoodMD\Controller\Adminhtml\AbstractAction;
use Eguana\GoodMD\Model\Log;
use Magento\Backend\App\Action\Context;

/**
 * Class Delete
 * @package Eguana\GoodMD\Controller\Adminhtml\Log
 */
class Delete extends AbstractAction
{
    public $goodMDLogModel;

    public function __construct(
        Context $context,
        Log $goodMDLogModel
    ) {
        $this->goodMDLogModel = $goodMDLogModel;
        parent::__construct($context);
    }

    /**
     * Delete banner action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = (int)$this->getRequest()->getParam('entity_id');
        if ($id) {
            try {
                $this->goodMDLogModel->load($id)->delete();
                $this->messageManager->addSuccessMessage(__('row was successfully deleted'));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }
        $this->messageManager->addErrorMessage(__('row could not be deleted'));
        return $resultRedirect->setPath('*/*/index');
    }
}
