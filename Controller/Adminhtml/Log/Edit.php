<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:10
 */
namespace Eguana\GoodMD\Controller\Adminhtml\Log;

use Eguana\GoodMD\Controller\Adminhtml\AbstractAction;
use Eguana\GoodMD\Model\Log;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;

class Edit extends AbstractAction
{
    public $coreRegistry;

    public $goodMDLogModel;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Log $goodMDLogModel
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->goodMDLogModel = $goodMDLogModel;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->goodMDLogModel;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This log no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->coreRegistry->register('eguana_goodmd_log', $model);

        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Log') : __('New Log'),
            $id ? __('Edit Log') : __('New Log')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Log'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Log'));
        return $resultPage;
    }
}
