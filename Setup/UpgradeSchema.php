<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-06-13
 * Time: 오전 9:05
 */

namespace Eguana\GoodMD\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.1', '<=')) {
            $this->goodmdLogTableCreate($setup);
            $this->orderItemTableColumnAdd($setup);
        }

        if (version_compare($context->getVersion(), '0.1.2', '<=')) {
            $this->goodmdLogTableColumnAdd($setup);
        }

        $setup->endSetup();
    }

    /**
     * create goodmd log table
     *
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    public function goodmdLogTableCreate(SchemaSetupInterface $setup)
    {
        $tableName = 'eguana_goodmd_log';
        if (!$setup->tableExists($tableName)) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable($tableName)
            )
                ->addColumn(
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'interface_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable => false'],
                    'Interface Name'
                )
                ->addColumn(
                    'goodmd_data',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '2M',
                    ['nullable' => false],
                    'GoodMD DATA'
                )
                ->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    2,
                    [
                        'nullable' => false,
                        'default'   => '01'
                    ],
                    'Log Status'
                )->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Log Created Time'
                )
                ->addColumn(
                    'updated_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Log updated Time'
                )
                ->addIndex(
                    $setup->getIdxName($tableName, ['entity_id']),
                    ['entity_id']
                );

            $setup->getConnection()->createTable($table);
        }
        $setup->endSetup();
    }

    /**
     * add interface status column add in sales order item table
     *
     * @param SchemaSetupInterface $setup
     */
    public function orderItemTableColumnAdd(SchemaSetupInterface $setup)
    {
        /**
         * @var $installer SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_item'),
            'interface_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 2,
                'nullable' => true,
                'default' => '01',
                'comment' => 'Interface Status'
            ]
        );
    }

    /**
     * add msg column in eguana goodmd log table
     *
     * @param SchemaSetupInterface $setup
     */
    public function goodmdLogTableColumnAdd(SchemaSetupInterface $setup)
    {
        /**
         * @var $installer SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('eguana_goodmd_log'),
            'message',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'Interface Message'
            ]
        );
    }
}
