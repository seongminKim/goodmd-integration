<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-30
 * Time: 오후 1:48
 */

namespace Eguana\GoodMD\Model;

use Exception;
use Magento\Framework\Exception\FileSystemException as FilesystemExceptionAlias;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * AbstractModel
 *
 * get Post Data from goodMD about Product,Shipment,Stock
 */
class GoodMDAbstractModel
{
    const ORDER = 'Order';

    const PRODUCT = 'Product';

    const STOCK = 'Stock';

    const SHIPMENT = 'Shipment';

    const SUCCESS = '02';

    const FAIL = '03';

    /**
     * @var File
     */
    private $file;
    /**
     * @var Json
     */
    public $json;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var TimezoneInterface
     */
    public $timezone;
    /**
     * @var Product
     */
    private $product;
    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * GoodMDAbstractModel constructor.
     * @param File $file
     * @param Json $json
     * @param ProductRepository $productRepository
     * @param TimezoneInterface $timezone
     * @param Product $product
     * @param ProductFactory $productFactory
     */
    public function __construct(
        File $file,
        Json $json,
        ProductRepository $productRepository,
        TimezoneInterface $timezone,
        Product $product,
        ProductFactory $productFactory
    ) {
        $this->file = $file;
        $this->json = $json;
        $this->productRepository = $productRepository;
        $this->timezone = $timezone;
        $this->product = $product;
        $this->productFactory = $productFactory;
    }

    /**
     * get JSON Data from GoodMD Request
     */
    public function getPostData()
    {
        try {
            $requestData = $this->file->fileGetContents("php://input");
        } catch (FilesystemExceptionAlias $e) {
            $requestData = $e;
        }

        return $requestData;
    }

    /**
     * @param $sku string
     * @return Product | false
     */
    public function _getProduct($sku)
    {
        try {
            $product = $this->productRepository->get($sku, false);
        } catch (NoSuchEntityException $e) {
            return false;
        }

        return $product;
    }

    /**
     * @param $sku string
     * @param $name
     * @param $price
     * @return Product
     */
    public function _getNewProduct($sku, $name, $price)
    {
        /** @var $product Product */
        $product = $this->productFactory->create();

        $product->setSku($sku);
        $product->setTypeId('simple');
        $product->setData('name', $name);
        $product->setData('price', $price);
        $product->setStatus(2);
        $product->setData('visibility', 4);
        $product->setAttributeSetId(10);

        return $product;
    }

    /**
     * @param $product
     * @param $name
     * @param $price
     * @return mixed
     */
    public function _updateProductData($product, $name, $price)
    {
        $product->setData('name', $name);
        $product->setData('price', $price);

        return $product;
    }

    /**
     * @param $product Product
     * @return array
     */
    public function _saveProduct($product)
    {
        try {
            $product->save();
            $result = ['result' => true, 'Message' => 'SUCCESS'];
        } catch (Exception $e) {
            $result = ['result' => false, 'Message' => $e->getMessage()];
        }

        return $result;
    }

    /**
     * Save Log data in eguana_goodmd_log table
     *
     * status
     * 1 = pending
     * 2 = success
     * 3 = fail
     *
     * @param $logFactory
     * @param $interface_name
     * @param $jsonData
     * @param $status
     * @param $message
     * @throws Exception
     */
    public function goodmdLogSave($logFactory, $interface_name, $jsonData, $status, $message)
    {
        /** @var $logModel Log */
        $logModel = $logFactory->create();

        $logModel->setData('interface_name', $interface_name);
        $logModel->setData('goodmd_data', $jsonData);
        $logModel->setData('status', $status);
        $logModel->setData('message', $message);

        $logModel->save();
    }

    /**
     * binding response data
     *
     * @param $resultMessage
     * @param $sku
     * @param $result
     *
     * @return array
     */
    public function responseData($sku, $result, $resultMessage)
    {
        $data = [
            'COMPANY_GOODS_CD' => $sku,
            'RESULT' => $result,
            'MSG' => $resultMessage,
        ];

        return $data;
    }

    /**
     * Validate Creating product successfully completed
     *
     * @param $data
     * @param $interface
     * @return array
     */
    public function successValidation($data, $interface)
    {
        $validation = 'S';
        $message = 'Success';

        if ($interface == self::PRODUCT || $interface == self::STOCK) {
            foreach ($data['DATA'] as $key => $value) {
                if ($value['RESULT'] === 'FAIL') {
                    $validation = 'E';
                    $message = 'Please check product #'.$value['COMPANY_GOODS_CD'];
                }
            }
        }

        if ($interface == self::SHIPMENT) {
            foreach ($data['DATA'] as $key => $value) {
                if ($value['RESULT'] === 'FAIL') {
                    $validation = 'E';
                    $message = 'Please check shipment Order#'.$value['IDX'];
                }
            }
        }

        $result = [
            'validation' => $validation,
            'message' => $message
        ];

        return $result;
    }
}
