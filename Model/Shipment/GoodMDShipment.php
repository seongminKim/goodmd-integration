<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-30
 * Time: 오후 3:24
 */

namespace Eguana\GoodMD\Model\Shipment;

use Eguana\GoodMD\Api\GoodMDShipmentInterface;
use Eguana\GoodMD\Model\GoodMDAbstractModel;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Sales\Model\Convert\Order as ConverOrder;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Eguana\GoodMD\Model\LogFactory;

/**
 * class GoodMDShipment
 *
 * create Shipment using json data
 * increment Id, tracking number, carrier code
 */
class GoodMDShipment extends GoodMDAbstractModel implements GoodMDShipmentInterface
{
    /**
     * @var TrackFactory
     */
    private $trackFactory;
    /**
     * @var ConverOrder
     */
    private $convertOrder;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;
    /**
     * @var LogFactory
     */
    private $logFactory;
    /**
     * @var ItemRepository
     */
    private $orderItemRepository;
    /**
     * @var Magento\Shipping\Model\ShipmentNotifier
     */
    private $shipmentNotifier;
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory
     */
    private $shipmentCollection;
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory
     */
    private $itemCollection;

    public function __construct(
        File $file,
        Json $json,
        ProductRepository $productRepository,
        TimezoneInterface $timezone,
        Product $product,
        ProductFactory $productFactory,
        ItemRepository $orderItemRepository,
        ConverOrder $convertOrder,
        TrackFactory $trackFactory,
        OrderRepositoryInterface $orderRepository,
        ShipmentRepositoryInterface $shipmentRepository,
        LogFactory $logFactory,
        \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier,
        \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentCollection,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $itemCollection
    ) {
        parent::__construct($file, $json, $productRepository, $timezone, $product, $productFactory);
        $this->trackFactory = $trackFactory;
        $this->convertOrder = $convertOrder;
        $this->orderRepository = $orderRepository;
        $this->shipmentRepository = $shipmentRepository;
        $this->logFactory = $logFactory;
        $this->orderItemRepository = $orderItemRepository;
        $this->shipmentNotifier = $shipmentNotifier;
        $this->shipmentCollection = $shipmentCollection;
        $this->itemCollection = $itemCollection;
    }

    /**
     * get Shipment json data
     */
    public function shipmentGet()
    {
        $requestData = $this->getPostData();

        $jsonDecode = $this->json->unserialize($requestData);

        $item = $jsonDecode['item'];

        $data = $this->unbindData($item);

        $validation = $this->successValidation($data, self::SHIPMENT);

        $jsonData = json_encode($data);

        if ($validation['validation'] === 'S') {
            $this->goodmdLogSave($this->logFactory, self::SHIPMENT, $requestData, self::SUCCESS, $validation['message']);
        } else {
            $this->goodmdLogSave($this->logFactory, self::SHIPMENT, $requestData, self::FAIL, $validation['message']);
        }

        return $jsonData;
    }

    /**
     * Unbind json data and create product
     *
     * @param $item
     * @return array|string
     * @throws LocalizedException
     */
    public function unbindData($item)
    {
        $resultData = [];

        $header = [
            'SEND_COMPANY_ID' => 'EguanaCommerce',
            'SEND_DATE' => $this->timezone->date(null, 'ko_KR', false)->format('YmdHis'),
            'TOTAL_COUNT' => count($item)
        ];

        if (isset($item)) {
            foreach ($item as $key => $value) {
                $errorMsg = $this->validationShipmentData($value);

                $item_id = $value['idx'];
                $carrier_code = $value['delivery_id'];
                $tracking_number = $value['invoice_no'];

                $item = $this->orderItemRepository->get($item_id);
                $order_id = $item->getOrderId();
                $order = $this->orderRepository->get($order_id);

                if ($errorMsg) {
                    $resultData[] = $this->responseShipData($item_id, 'FAIL', $errorMsg);
                    continue;
                }

                $result = $this->createShipment($item, $order, $carrier_code, $tracking_number);

                if ($result['result']) {
                    $resultData[] = $this->responseShipData($item_id, 'SUCCESS', $result['Message']);
                    $this->setShipmentStatus($order);
                } else {
                    $resultData[] = $this->responseShipData($item_id, 'FAIL', $result['Message']);
                }
            }

            $bodyData = [
                'DATA' => $resultData
            ];

            $data = array_merge($header, $bodyData);

            return $data;
        }

        return 'Shipment data is empty';
    }

    /**
     * Validate OrderId, CarrierCode, TrackingNumber exist
     *
     * @param $value
     * @return string
     */
    public function validationShipmentData($value)
    {
        $msg = '';
        $order_id = $value['idx'];
        $carrier_code = $value['delivery_id'];
        $tracking_number = $value['invoice_no'];

        if (!$order_id) {
            $msg = 'IDX Item Id Invalid';
        }

        if (!$carrier_code) {
            $msg = 'Delivery Id Invalid';
        }

        if (!$tracking_number) {
            $msg = 'Invoice No Invalid';
        }

        return $msg;
    }

    /**
     * Create shipment
     *
     * @param $item
     * @param $order
     * @param $carrier_code
     * @param $tracking_number
     * @return array
     * @throws LocalizedException
     */
    public function createShipment($item, $order, $carrier_code, $tracking_number)
    {

        $carrier_title = $this->getCarrierTitle($carrier_code);

        $trackData = [
            'carrier_code' => $carrier_code,
            'title' => $carrier_title,
            'number' => $tracking_number
        ];

        if ($order->canShip() && $order->getState() =='processing') {
            $shipment = $this->convertOrder->toShipment($order);

            $qtyToShipped = $item->getQtyToShip();

            $shipmentItem = $this->convertOrder->itemToShipmentItem($item)->setQty($qtyToShipped);

            $shipment->addItem($shipmentItem);

            $trackFactory = $this->trackFactory->create();
            $trackFactory->addData($trackData);

            $shipment->addTrack($trackFactory);
            $shipment->register();

            try {
                $this->shipmentRepository->save($shipment);
                $this->orderRepository->save($shipment->getOrder());
                $this->shipmentNotifier->notify($shipment);

                $result = ['result' => true, 'Message' =>'SUCCESS'];
            } catch (\Exception $e) {
                $result = ['result' => false, 'Message' =>$e->getMessage()];
            }
        } else {
            $result = ['result' => false, 'Message' =>'Shipment creation fail, please check status'];
        }

        return $result;
    }

    /**
     * binding response data
     *
     * @param $item_id
     * @param $result
     * @param $resultMessage
     * @return array
     */
    public function responseShipData($item_id, $result, $resultMessage)
    {
        $data = [
            'IDX' => $item_id,
            'RESULT' => $result,
            'MSG' => $resultMessage,
        ];

        return $data;
    }

    /**
     * Update order state, status
     *
     * Update order state and status to complete if total shipment qty is same as order total qty
     * @param $order
     * @param $order_id
     */
    public function setShipmentStatus($order)
    {
        $shipmentCollection = $this->shipmentCollection->create()->addFieldToFilter('order_id', $order->getId());
        $orderItemCollection = $this->itemCollection->create()->addFieldToFilter('order_id', $order->getId());

        $totalShipQty = 0;
        $orderTotalQty = 0;

        foreach ($shipmentCollection->getItems() as $data) {
            $totalShipQty += $data->getTotalQty();
        }

        foreach ($orderItemCollection->getItems() as $item) {
            $orderTotalQty += $item->getQtyOrdered();
        }

        if ($totalShipQty == $orderTotalQty) {
            $order->setStatus('complete');
            $order->setState('complete');
        }
        $this->orderRepository->save($order);
    }

    /**
     * Mapping Carrier code and Carrier title
     * @param $carrier_code
     */
    public function getCarrierTitle($carrier_code)
    {
        $carrier_title = '';

        if ($carrier_code == '201904820') {
            $carrier_title = 'Pantos Logistics';
        } elseif ($carrier_code == '201904821') {
            $carrier_title = 'Qxpress';
        } elseif ($carrier_code == '201907860') {
            $carrier_title = 'EMS';
        } elseif ($carrier_code == '201907861') {
            $carrier_title = 'DHL';
        }

        return $carrier_title;
    }
}
