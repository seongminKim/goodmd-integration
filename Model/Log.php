<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-06-13
 * Time: 오전 9:17
 */

namespace Eguana\GoodMD\Model;

use Eguana\GoodMD\Api\Data\GoodMDLogInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * @method ResourceModel\Log getResource()
 */
class Log extends AbstractModel implements GoodMDLogInterface
{
    public function _construct()
    {
        $this->_init('Eguana\GoodMD\Model\ResourceModel\Log');
    }
    /**
     * @param int $entity_id
     * @return $this
     */
    public function setEntityId($entity_id)
    {
        return $this->setData($entity_id, self::ENTITY_ID);
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @param $interfaceName
     * @return $this
     */
    public function setInterfaceName($interfaceName)
    {
        return $this->setData(self::INTERFACE_NAME, $interfaceName);
    }

    /** @return string */
    public function getInterfaceName()
    {
        return $this->getData(self::INTERFACE_NAME);
    }

    /**
     * @param $goodmdData
     * @return $this
     */
    public function setGOODMDdata($goodmdData)
    {
        return $this->setData(self::GOODMD_DATA, $goodmdData);
    }

    /** @return string */
    public function getGOODMDdata()
    {
        return $this->getData(self::GOODMD_DATA);
    }

    /**
     * @param $status string
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /** @return string */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param $message string
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /** @return string */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }
}
