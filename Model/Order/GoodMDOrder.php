<?php

/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-28
 * Time: 오전 9:30
 */

namespace Eguana\GoodMD\Model\Order;

use Eguana\GoodMD\Model\GoodMDAbstractModel;
use Eguana\GoodMD\Api\GoodMDOrderInterface;
use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\Timezone;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Company\Api\CompanyManagementInterface;
use Magento\Directory\Model\CountryFactory;
use Eguana\GoodMD\Model\LogFactory;
use Magento\Catalog\Model\ProductRepository;

/**
 * class GoodMDOrder
 *
 * binding order data to provide
 */
class GoodMDOrder extends GoodMDAbstractModel implements GoodMDOrderInterface
{

    /**
     * @var OrderItemRepositoryInterface
     */
    private $orderItemRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var TimezoneInterface
     */
    private $date;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var CompanyManagementInterface
     */
    private $companyManagement;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var CountryFactory
     */
    private $countryFactory;
    /**
     * @var LogFactory
     */
    public $logFactory;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;

    /**
     * GoodMDOrder constructor.
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param TimezoneInterface $date
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param StoreManagerInterface $storeManager
     * @param CompanyManagementInterface $companyManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param CountryFactory $countryFactory
     * @param LogFactory $logFactory
     * @param ProductRepository $productRepository
     * @param Timezone $dateTime
     */
    public function __construct(
        OrderItemRepositoryInterface $orderItemRepository,
        TimezoneInterface $date,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        StoreManagerInterface $storeManager,
        CompanyManagementInterface $companyManagement,
        OrderRepositoryInterface $orderRepository,
        CountryFactory $countryFactory,
        LogFactory $logFactory,
        ProductRepository $productRepository,
        Timezone $dateTime
    ) {
        $this->orderItemRepository = $orderItemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->date = $date;
        $this->storeManager = $storeManager;
        $this->companyManagement = $companyManagement;
        $this->orderRepository = $orderRepository;
        $this->countryFactory = $countryFactory;
        $this->logFactory = $logFactory;
        $this->productRepository = $productRepository;
        $this->dateTime = $dateTime;
    }

    /**
     * Return bind Order data
     * @return string
     * @throws NoSuchEntityException
     */
    public function orderGet()
    {
        $data = $this->bindData();

        if (!empty($data)) {
            $jsonData = json_encode($data);

            try {
                $message = 'Success to bind data';
                $this->goodmdLogSave($this->logFactory, self::ORDER, $jsonData, self::SUCCESS, $message);
                $this->setItemStatus(self::SUCCESS, $data);

                return $jsonData;
            } catch (Exception $e) {
                $message = 'Fail to bind data';
                $this->goodmdLogSave($this->logFactory, self::ORDER, $jsonData, self::FAIL, $message);
                $this->setItemStatus(self::FAIL, $data);

                return 'Error';
            }
        }
        return '';
    }

    /**
     * Binding Order data
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function bindData()
    {
        $itemData = $this->getItemData();
        $items = $itemData['data'];
        $itemCount = $itemData['count'];

        $storeCode = $this->storeManager->getStore()->getCode();
        $date = $this->dateTime->date()->format('YmdHis');

        $header = [
            'SEND_COMPANY_ID' =>'EguanaCommerce',
            'SEND_DATE' => $date,
            'TOTAL_COUNT' =>$itemCount
        ];

        if ($itemCount == 0) {
            $bodyData = [
                'DATA' => []
            ];

            $data = array_merge($header, $bodyData);
        } else {
            $body = [];
            $reward = [];
            $credit = [];

            foreach ($items as $item) {
                $order = $item->getOrder();

                $shippingAddress = $order->getShippingAddress();
                $billingAddress = $order->getBillingAddress();
                $countryName = $this->countryFactory->create()->loadByCode($shippingAddress->getCountryId())->getName();

                $orderIncrementId = $order->getIncrementId();
                $orderSubtotal = $order->getSubtotal();
                $time = $order->getData('created_at');
                $orderCreatedAt = $this->dateTime->date($time)->format('Y-m-d H:i:s');
                $customerId = $order->getCustomerId();
                $orderName = $order->getData('customer_firstname').$order->getData('customer_lastname');
                $orderState = $order->getState();
                $deliveryMessage = ($order->getDeliveryMessage()!==null)? $order->getDeliveryMessage() : '';
                $orderId = $order->getId();
                $shippingTitle = $this->getShippingTitle($order);

                $itemId = $item->getItemId();
                $sku = $item->getSku();
                $name = $item->getName();
                $itemPrice = $this->getItemPriceValid($item, $customerId);
                $itemQtyOrdered = (int)$item->getQtyOrdered();
                $itemRowTotal = $item->getRowTotal();
                $itemDiscountAmount = number_format(floor($item->getDiscountAmount()*100) / 100, 2, '.', '');
                $taxAmount = $item->getTaxAmount();

                $total_cost = number_format($itemRowTotal - $itemDiscountAmount + $taxAmount, 2, '.', '');
                $rewardPoint= $order->getRewardCurrencyAmount();
                $storeCredit= $order->getCustomerBalanceAmount();
                $orderShippingAmount = $this->shippingAmountValid($order, $customerId, $itemPrice, $orderSubtotal);

                $calculateReward = $this->calculateCost($rewardPoint, $orderSubtotal, $itemRowTotal);
                $calculateStoreCredit = $this->calculateCost($storeCredit, $orderSubtotal, $itemRowTotal);

                $reward[$orderId][] = $calculateReward;
                $credit[$orderId][] = $calculateStoreCredit;

                $mall_user_id = $this->validateCompany($customerId);

                $body[] = [
                    'IDX' => $itemId,
                    'ORDER_ID' => $orderIncrementId,
                    'MALL_ID' => $storeCode,
                    'MALL_USER_ID' => $mall_user_id,
                    'ORDER_STATUS' => $orderState,
                    'USER_NAME' => ($orderName!=='')? $orderName : 'GUEST' ,
                    'USER_TEL' =>$billingAddress->getTelephone(),
                    'USER_CEL' => $billingAddress->getTelephone(),
                    'USER_EMAIL' => $billingAddress->getEmail(),
                    'RECEIVE_TEL' => $shippingAddress->getTelephone(),
                    'RECEIVE_CEL' => $shippingAddress->getTelephone(),
                    'RECEIVE_NAME' => $shippingAddress->getName(),
                    'RECEIVE_ZIPCODE' => ($shippingAddress->getPostcode()!==null) ? $shippingAddress->getPostcode() : '000',
                    'RECEIVE_COUNTRY' => $countryName,
                    'RECEIVE_STATE' => $shippingAddress->getCity(),
                    'RECEIVE_REGION' => $shippingAddress->getRegion(),
                    'RECEIVE_ADDR' => $shippingAddress->getStreetLine(1).$shippingAddress->getStreetLine(2).$shippingAddress->getStreetLine(3),
                    'TOTAL_COST' => $total_cost,
                    'MILE_COST' => $calculateReward,
                    'COUP_COST' => $itemDiscountAmount,
                    'CREDIT_COST' => $calculateStoreCredit,
                    'ORDER_DATE' => $orderCreatedAt,
                    'MALL_PRODUCT_ID' => $sku,
                    'PRODUCT_NAME' => $name,
                    'MALL_WON_COST' => $itemPrice,
                    'SALE_CNT' => $itemQtyOrdered,
                    'DELIVERY_METHOD_STR' => 'N',
                    'DELV_COST' => $orderShippingAmount,
                    'COMPAYNY_GOODS_CD' => $sku,
                    'REG_DATE' => $date,
                    'DELV_MSG' => $deliveryMessage,
                    'CURRENCY' => $order->getStoreCurrencyCode(),
                    'DELIVERY_ID' => $shippingTitle
                ];
            }

            $bodyItem = $this->addRemainCost($reward, $credit, $body);

            $bodyData = [
                'DATA' => $bodyItem
            ];

            $data = array_merge($header, $bodyData);
        }
        return $data;
    }

    /**
     * Add remain reward, store credit to first item
     *
     * @param $reward
     * @param $credit
     * @param $body
     * @return array
     */
    public function addRemainCost($reward, $credit, $body)
    {
        foreach ($reward as $id => $cost) {
            $sumCost[$id] = array_sum($cost);
            $order = $this->orderRepository->get($id);

            foreach ($body as $key => $value) {
                if ($value['ORDER_ID'] == $id) {
                    if ($sumCost[$id] !==0.00) {
                        $body[$key]['MILE_COST'] = (string)($value['MILE_COST'] +($order->getRewardCurrencyAmount() - $sumCost[$id]));
                    } else {
                        $body[$key]['MILE_COST'] = number_format(0.00, 2, '.', '');
                    }
                    break;
                }
            }
        }

        foreach ($credit as $id => $cost) {
            $sumCost[$id] = array_sum($cost);
            $order = $this->orderRepository->get($id);

            foreach ($body as $key => $value) {
                if ($value['ORDER_ID'] == $id) {
                    if ($sumCost[$id] !==0.00) {
                        $body[$key]['CREDIT_COST']= (string)($value['CREDIT_COST'] + ($order->getCustomerBalanceAmount() - $sumCost[$id]));
                    } else {
                        $body[$key]['CREDIT_COST'] = number_format(0.00, 2, '.', '');
                    }

                    break;
                }
            }
        }

        return $body;
    }

    /**
     * Search Item Data by order id
     * Get Order Id only state is processing
     *
     * @return OrderItemInterface[]
     */
    public function getItemData()
    {
        $search = $this->searchCriteriaBuilder->addFilter('state', 'processing')->create();
        $orders = $this->orderRepository->getList($search)->getItems();
        $orderIds = [];
        foreach ($orders as $order) {
            $orderIds[] = $this->filterBuilder
                ->setField('order_id')
                ->setConditionType('eq')
                ->setValue($order->getId())->create();
        }

        if (!empty($orderIds)) {
            $itemData = $this->getItemSearchCriteria($orderIds);
        } else {
            $itemData['data'] = '';
            $itemData['count'] = 0;
        }
        return $itemData;
    }

    /**
     * Search Item Data
     *
     * Get item data only interface status is 01(before sending)
     * @param $orderIds
     * @return OrderItemInterface[]
     */
    public function getItemSearchCriteria($orderIds)
    {
        $searchItemFilter = $this->searchCriteriaBuilder->addFilters($orderIds);
        $searchItemFilter->addFilter('interface_status', '01');
        $searchItems = $searchItemFilter->create();
        $itemData['data'] = $this->orderItemRepository->getList($searchItems)->getItems();
        $itemData['count'] = $this->orderItemRepository->getList($searchItems)->getTotalCount();

        return $itemData;
    }

    /**
     * Set interface_status '01' -> '02'
     *
     * interface status
     * 01 = before send
     * 02 = after send
     *
     * @param $status
     * @param $data
     */
    public function setItemStatus($status, $data)
    {
        $orderData = $data['DATA'];

        if ($orderData!=='') {
            foreach ($orderData as $key => $value) {
                $item_id = $value['IDX'];
                $orderItem = $this->orderItemRepository->get($item_id);

                $orderItem->setData('interface_status', $status);

                $this->orderItemRepository->save($orderItem);
            }
        }
    }

    /**
     * Calculate cost according to item row total rate
     *
     * @param $cost
     * @param $totalSub
     * @param $itemRowTotal
     * @return float|int
     */
    public function calculateCost($cost, $totalSub, $itemRowTotal)
    {
        $calculatePoint = number_format(0.00, 2, '.', '');

        if ($cost!==null) {
            //point calculate according item subtotal rate
            $point = ($cost / $totalSub) * $itemRowTotal;

            //for second decimal place
            $calculatePoint = number_format(floor($point*100) / 100, 2, '.', '');
        }

        return $calculatePoint;
    }

    /**
     * Validate user using customer id whether affiliated in company
     *
     * @param $customerId
     * @return string
     */
    public function validateCompany($customerId)
    {
        $mall_user = 'COSRX';

        if ($customerId!==null) {
            $mall_user = ($this->getCompanyName($customerId)!=='')?$this->getCompanyName($customerId):'COSRX';
        }

        return $mall_user;
    }

    /**
     * Search Company Name
     *
     * @param $customerId
     * @return string
     */
    public function getCompanyName($customerId)
    {
        $companyName = '';
        $company = $this->companyManagement->getByCustomerId($customerId);

        if ($company !== null) {
            $companyName = $company->getCompanyName();
        }
        return $companyName;
    }

    /**
     * Shipping Amount validation
     *
     * B2B customer ordered by quote , shipping amount will be charge subtotal - item price
     *
     * @param $order
     * @param $customerId
     * @param $itemPrice
     * @param $subtotal
     * @return string
     */
    public function shippingAmountValid($order)
    {
        $shippingAmount = number_format($order->getShippingAmount(), 2, '.', '');

        return $shippingAmount;
    }

    /**
     * Item price validation
     *
     * B2C, B2B item price and special price
     * @param $item
     * @param $customerId
     * @return float|int|string
     * @throws NoSuchEntityException
     */
    public function getItemPriceValid($item, $customerId)
    {
        $price = 0;

        if ($customerId!==null) {
            //b2c
            $itemPrice = number_format($item->getPrice(), 2, '.', '');

            //b2b
            if ($this->getCompanyName($customerId) !== '') {
                $product = $this->productRepository->get($item->getSku());
                if ($product) {
                    $special_price = $product->getSpecialPrice();
                    if ($special_price !== null) {
                        if ($item->getOriginalPrice() > $special_price) {
                            $price = $special_price;
                        } else {
                            $price = $item->getOriginalPrice();
                        }
                    } else {
                        $price = $item->getOriginalPrice();
                    }
                }

                $itemPrice = $price;
            }
        } else {
            $itemPrice = number_format($item->getPrice(), 2, '.', '');
        }
        return $itemPrice;
    }

    public function getShippingTitle($order)
    {
        $shippingDescription = $order->getShippingDescription();

        $shippingTitle = explode('-', $shippingDescription);

        return trim($shippingTitle[0]);
    }

}
