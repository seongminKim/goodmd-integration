<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-06-13
 * Time: 오전 10:10
 */

namespace Eguana\GoodMD\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Status implements OptionSourceInterface
{
    const SUCCESS = '02';

    const FAIL = '03';
    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            ['value'=>self::SUCCESS, 'label'=>'Success'],
            ['value'=>self::FAIL, 'label'=>'Fail']
        ];
    }
}
