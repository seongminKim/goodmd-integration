<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-06-13
 * Time: 오전 10:01
 */

namespace Eguana\GoodMD\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class InterfaceName implements OptionSourceInterface
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'Order', 'label' =>'Order Interface'],
            ['value' => 'Product', 'label' =>'Product Interface'],
            ['value' => 'Stock', 'label' =>'Stock Interface'],
            ['value' => 'Shipment', 'label' =>'Shipment Interface']
        ];
    }
}
