<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-28
 * Time: 오전 9:33
 */

namespace Eguana\GoodMD\Model\Stock;

use Eguana\GoodMD\Api\GoodMDStockInterface;
use Eguana\GoodMD\Model\GoodMDAbstractModel;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Eguana\GoodMD\Model\LogFactory;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;

/**
 * class GoodMDStock
 *
 * update Stock by json data
 * sku, stock
 */
class GoodMDStock extends GoodMDAbstractModel implements GoodMDStockInterface
{
    /**
     * @var LogFactory
     */
    private $logFactory;
    /**
     * @var SourceItemInterface
     */
    private $sourceItem;
    /**
     * @var SourceItemsSaveInterface
     */
    private $sourceItemsSave;

    public function __construct(
        File $file,
        Json $json,
        ProductRepository $productRepository,
        TimezoneInterface $timezone,
        Product $product,
        ProductFactory $productFactory,
        LogFactory $logFactory,
        SourceItemInterface $sourceItem,
        SourceItemsSaveInterface $sourceItemsSave
    ) {
        parent::__construct($file, $json, $productRepository, $timezone, $product, $productFactory);
        $this->logFactory = $logFactory;
        $this->sourceItem = $sourceItem;
        $this->sourceItemsSave = $sourceItemsSave;
    }

    /**
     * get Stock json data
     */
    public function stockGet()
    {
        $requestData = $this->getPostData();

        $jsonDecode = $this->json->unserialize($requestData);

        $item = $jsonDecode['item'];

        $data = $this->unbindData($item);

        $validation = $this->successValidation($data, self::STOCK);

        $jsonData = json_encode($data);

        if ($validation['validation'] === 'S') {
            $this->goodmdLogSave($this->logFactory, self::STOCK, $requestData, self::SUCCESS, $validation['message']);
        } else {
            $this->goodmdLogSave($this->logFactory, self::STOCK, $requestData, self::FAIL, $validation['message']);
        }

        return $jsonData;
    }

    /**
     * Unbind json data and create product
     *
     * @param $item
     * @return array|string
     */
    public function unbindData($item)
    {
        $resultData = [];

        $header = [
                'SEND_COMPANY_ID' =>'EguanaCommerce',
                'SEND_DATE' => $this->timezone->date(null, 'ko_KR', false)->format('YmdHis'),
                'TOTAL_COUNT' =>count($item)
        ];

        if (isset($item)) {
            foreach ($item as $key => $value) {
                $errorMsg = $this->validationStockData($value);

                $sku = $value['company_goods_cd'];
                $qty = $value['qty'];

                if ($errorMsg) {
                    $resultData[] = $this->responseData($sku, 'FAIL', $errorMsg);
                    continue;
                }

                $product = $this->_getProduct($sku);

                if (!$product) {
                    $resultData[] = $this->responseData($sku, 'FAIL', 'Product does not exist');
                    continue;
                }

                if ($qty < 0) {
                    $qty = 0;
                }

                $result = $this->setQtyToProduct($product, $qty);

                if ($result['result']) {
                    $resultData[] = $this->responseData($sku, 'SUCCESS', $result['Message']);
                } else {
                    $resultData[] = $this->responseData($sku, 'FAIL', $result['Message']);
                }
            }

            $bodyData = [
                'DATA' => $resultData
            ];

            $data = array_merge($header, $bodyData);

            return $data;
        }

        return 'Stock data is empty';
    }

    /**
     * Validate Sku, Qty exist
     *
     * @param $value
     * @return string
     */
    public function validationStockData($value)
    {
        $msg ='';
        $sku = $value['company_goods_cd'];
        $qty = $value['qty'];

        if (!$sku) {
            $msg = 'Sku invalid';
        }

        if (!$qty) {
            $msg = 'Name invalid';
        }

        return $msg;
    }

    /**
     * Set Stock qty
     *
     * @param $product
     * @param $qty
     * @return array
     */
    public function setQtyToProduct($product, $qty)
    {
        try {
            $this->sourceItem->setSku($product->getSku());
            $this->sourceItem->setQuantity($qty);
            $this->sourceItem->setStatus(1);
            if ($qty == 0) {
                $this->sourceItem->setStatus(0);
            }
            $this->sourceItem->setSourceCode('default');
            $this->sourceItemsSave->execute([$this->sourceItem]);

            $result = ['result' => true, 'Message' =>'SUCCESS'];
        } catch (\Exception $e) {
            $result = ['result' => false, 'Message' =>$e->getMessage()];
        }

        return $result;
    }
}
