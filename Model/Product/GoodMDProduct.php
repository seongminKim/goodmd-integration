<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-28
 * Time: 오전 9:31
 */

namespace Eguana\GoodMD\Model\Product;

use Eguana\GoodMD\Api\GoodMDProductInterface;
use Eguana\GoodMD\Model\GoodMDAbstractModel;
use Eguana\GoodMD\Model\LogFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * class GoodMDProduct
 *
 * create product using json data
 * sku, name, price
 */
class GoodMDProduct extends GoodMDAbstractModel implements GoodMDProductInterface
{
    /**
     * @var LogFactory
     */
    private $logFactory;

    public function __construct(
        File $file,
        Json $json,
        ProductRepository $productRepository,
        TimezoneInterface $timezone,
        Product $product,
        ProductFactory $productFactory,
        LogFactory $logFactory
    ) {
        parent::__construct($file, $json, $productRepository, $timezone, $product, $productFactory);
        $this->logFactory = $logFactory;
    }

    /**
     * get Product json data
     *
     * @return array
     * @throws \Exception
     */
    public function productGet()
    {
        $requestData = $this->getPostData();

        $jsonDecode = $this->json->unserialize($requestData);

        $item = $jsonDecode['item'];

        $data = $this->unbindData($item);

        $validation = $this->successValidation($data, self::PRODUCT);

        $jsonData = json_encode($data);

        if ($validation['validation'] === 'S') {
            $this->goodmdLogSave($this->logFactory, self::PRODUCT, $requestData, self::SUCCESS, $validation['message']);
        } else {
            $this->goodmdLogSave($this->logFactory, self::PRODUCT, $requestData, self::FAIL, $validation['message']);
        }

        return $jsonData;
    }

    /**
     * Unbind json data and create product
     *
     * @param $item
     * @return array|string
     */
    public function unbindData($item)
    {
        $resultData = [];

        $header = [
                'SEND_COMPANY_ID' =>'EguanaCommerce',
                'SEND_DATE' => $this->timezone->date(null, 'ko_KR', false)->format('YmdHis'),
                'TOTAL_COUNT' =>count($item)
        ];

        if (isset($item)) {
            foreach ($item as $key => $value) {
                $errorMsg = $this->validationProductData($value);

                $sku = $value['company_goods_cd'];
                $name = $value['product_name'];
                $price = $value['sale_price'];

                if ($errorMsg) {
                    $resultData[] = $this->responseData($sku, 'FAIL', $errorMsg);
                    continue;
                }

                $product = $this->_getProduct($sku);

                if (!$product) {
                    $product = $this->_getNewProduct($sku, $name, $price);
                } else {
                    $product = $this->_updateProductData($product, $name, $price);
                }

                $result = $this->_saveProduct($product);

                if ($result['result']) {
                    $resultData[] = $this->responseData($sku, 'SUCCESS', $result['Message']);
                } else {
                    $resultData[] = $this->responseData($sku, 'FAIL', $result['Message']);
                }
            }

            $bodyData = [
                'DATA' => $resultData
            ];

            $data = array_merge($header, $bodyData);

            return $data;
        }
        return 'Item data is empty';
    }

    /**
     * Validate Sku, Name, Price exist
     *
     * @param $value
     * @return string
     */
    public function validationProductData($value)
    {
        $msg ='';
        $sku = $value['company_goods_cd'];
        $name = $value['product_name'];
        $price = $value['sale_price'];

        if (!$sku) {
            $msg = 'Sku invalid';
        }

        if (!$name) {
            $msg = 'Name invalid';
        }

        if ($price < 0) {
            $msg = 'Sale price invalid';
        }

        return $msg;
    }
}
