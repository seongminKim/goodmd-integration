<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-06-13
 * Time: 오전 9:17
 */

namespace Eguana\GoodMD\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Log extends AbstractDb
{
    public $_idFieldName = 'entity_id';

    public function _construct()
    {
        $this->_init('eguana_goodmd_log', 'entity_id');
    }
}
