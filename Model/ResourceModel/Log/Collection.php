<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-06-13
 * Time: 오전 9:17
 */

namespace Eguana\GoodMD\Model\ResourceModel\Log;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    public $_idFieldName = 'entity_id';

    public function _construct()
    {
        $this->_init('Eguana\GoodMD\Model\Log', 'Eguana\GoodMD\Model\ResourceModel\Log');
    }
}


