<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-30
 * Time: 오후 3:23
 */

namespace Eguana\GoodMD\Api;

/**
 * GoodMD Shipping information Data Interface
 *
 * request from GoodMD to Magento
 * @api
 */
interface GoodMDShipmentInterface
{
    /**
     * get Shipment Data
     * @return string
     */
    public function shipmentGet();
}
