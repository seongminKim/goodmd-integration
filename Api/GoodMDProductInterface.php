<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-28
 * Time: 오전 9:31
 */

namespace Eguana\GoodMD\Api;

/**
 * GoodMD Product Data Interface
 *
 * request from GoodMD to Magento
 * @api
 */
interface GoodMDProductInterface
{
    /**
     * get Product Data
     * @return string
     */
    public function productGet();
}
