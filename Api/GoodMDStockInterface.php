<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-28
 * Time: 오전 9:32
 */

namespace Eguana\GoodMD\Api;

/**
 * GoodMD Stock Data Interface
 *
 * request from GoodMD to Magento
 * @api
 */
interface GoodMDStockInterface
{
    /**
     * get Stock Data
     * @return string
     */
    public function stockGet();
}
