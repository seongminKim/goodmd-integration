<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:23
 */

namespace Eguana\GoodMD\Api\Data;

/**
 * interface log interface
 */
interface GoodMDLogInterface
{
    const ENTITY_ID = 'entity_id';

    const INTERFACE_NAME = 'interface_name';

    const GOODMD_DATA = 'goodmd_data';

    const STATUS = 'status';

    const MESSAGE = 'message';

    /**
     * @param int $entity_id
     * @return $this
     */
    public function setEntityId($entity_id);

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param $interfaceName
     * @return $this
     */
    public function setInterfaceName($interfaceName);

    /** @return string*/
    public function getInterfaceName();

    /**
     * @param $goodmdData
     * @return $this
     */
    public function setGOODMDdata($goodmdData);

    /** @return string*/
    public function getGOODMDdata();

    /**
     * @param $status string
     * @return $this
     */
    public function setStatus($status);

    /** @return string*/
    public function getStatus();

    /**
     * @param $message string
     * @return $this
     */
    public function setMessage($message);

    /** @return string*/
    public function getMessage();
}
