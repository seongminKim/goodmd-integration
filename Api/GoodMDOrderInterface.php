<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-05-28
 * Time: 오전 9:29
 */

namespace Eguana\GoodMD\Api;

/**
 * GoodMD Order Data Interface
 *
 * request from GoodMD to Magento
 * provide Order binding data
 * @api
 */
interface GoodMDOrderInterface
{
    /**
     * get Order Data
     * @return string
     */
    public function orderGet();
}
